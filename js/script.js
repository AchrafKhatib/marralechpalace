$(document).ready(function() {
  var scrollButton = $("#scroll-top");
  // Nice Scroll

  $("html").niceScroll();

  // Caching The Scroll Top Element

  $(window).scroll(function() {
    if ($(this).scrollTop() >= 700) {
      scrollButton.show();
    } else {
      scrollButton.hide();
    }
  });

  // Click On Button To Scroll Top

  scrollButton.click(function() {
    $("html,body").animate({ scrollTop: 0 }, 600);
  });

  /* Demo purposes only */
  $(".footer .hover").mouseleave(function() {
    $(this).removeClass("hover");
  });


  // Fakes the loading setting a timeout
  setTimeout(function() {
      $('body').addClass('loaded');
    }, 0);

  
  $(window).load(function() {
    $('#loading').hide();
  });

});
